import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MySimpleTask {

    public static void main(String[] args){
        System.out.println("Исходный адрес - name@email.domen");

        System.out.println(checkWithRegExp("_@domen"));
        System.out.println(checkWithRegExp("email."));
        System.out.println(checkWithRegExp("name"));


        System.out.println("\nНаш адрес - test@gmail.com");

        System.out.println(dumbCheck("_@.com"));
        System.out.println(dumbCheck("gmail"));
        System.out.println(dumbCheck("test"));

    }

    public static boolean checkWithRegExp(String userNameString){
        Pattern p = Pattern.compile("^[a-z_0-9_@_.-]{3,15}$");
        Matcher m = p.matcher(userNameString);
        return m.matches();
    }

    public static boolean dumbCheck(String userNameString){

        char[] symbols = userNameString.toCharArray();
        if(symbols.length < 3 || symbols.length > 15 ) return false;

        String validationString = "@abcdefghijklmnopqrstuvwxyz0123456789_";

        for(char c : symbols){
            if(validationString.indexOf(c)==-1) return false;
        }

        return true;
    }
} 